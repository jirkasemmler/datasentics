# Datasentics scrapper
Finds all the images on the website, downloads it and categorizes them in to folders based on text or color.

**Options**
Categorization by
 - colors
 - text presence
 
## Usage
`python datasentics.py <url> <operation>`

 - `url` is absolute URL with `/` at the end
 - `operation` is one of the available operations - `text` or `colors`
 
e.g. `python datasentics.py https://datasentics.com colors`

## Output
It creates a directory structure in folder `output` folders witch represent categories of selected classifications

## Installation
Install
- `urllib` package 
- `pytesseract` package 
- `pip3 requirements.txt` 