import unittest

from src.scrapper.scrapper import Scraper


class ScrapperTestcase(unittest.TestCase):

    def test_continue_validator(self):
        checker = Scraper('https://example.com/')
        checker.used_links = ['https://example.com/', 'https://example.com/home', 'https://example.com/career']

        self.assertFalse(checker.should_continue('notvalidurl'))
        self.assertFalse(checker.should_continue('https://example.com/home'))
        self.assertFalse(checker.should_continue('https://anotherexample.com/home'))
        self.assertFalse(checker.should_continue('tel:123456798'))
        self.assertFalse(checker.should_continue('mailto:john.doe@example.com'))
        self.assertTrue(checker.should_continue('https://example.com/new-page'))

    def test_generate_absolute_url(self):
        checker = Scraper('https://example.com/')
        home_absolute = 'https://example.com/home'
        self.assertEqual(checker._get_absolute_path('home'), home_absolute)
        self.assertEqual(checker._get_absolute_path('/home'), home_absolute)
        self.assertEqual(checker._get_absolute_path(home_absolute), home_absolute)


if __name__ == '__main__':
    unittest.main()
