import unittest
import urllib.request
from http.client import HTTPMessage
from unittest.mock import patch, MagicMock

from src.classifier.classifier import Classifier


class MyTestCase(unittest.TestCase):

    def test_duplicate(self):
        classifier = Classifier()
        self.assertFalse(classifier.is_duplicated('./data/lena.png'))
        self.assertTrue(classifier.is_duplicated('./data/lena.png'))

    def test_get_image(self):
        with patch('urllib.request') as mock:
            instance = mock.return_value
            instance.urlretrieve.return_value = ['foo', HTTPMessage()]

            classifier = Classifier()
            classifier.is_duplicated = MagicMock()
            classifier.is_duplicated.return_value = False
            classifier.get_images(['./data/lena.png'])

            classifier.is_duplicated.assert_called_once()
            self.assertEqual(len(classifier.saved_images), 1)


if __name__ == '__main__':
    unittest.main()
