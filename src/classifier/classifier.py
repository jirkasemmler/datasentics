import random
import string
import urllib.request
import os
import shutil
import pytesseract
import webcolors
from PIL import Image, ImageStat
from url_normalize import url_normalize


class Classifier:
    directory = './output'
    directory_data_src = 'src_data'

    directory_text = 'text'
    directory_no_text = 'no_text'

    saved_images = []
    images_not_reached = []

    hashed_images = []

    def __init__(self):
        # cleaning before running
        if os.path.exists(self.directory):
            shutil.rmtree(os.path.join(self.directory))

        os.mkdir(self.directory)
        os.mkdir(os.path.join(self.directory, self.directory_data_src))

    def __del__(self):
        shutil.rmtree(os.path.join(self.directory, self.directory_data_src))

    def get_images(self, images_urls):
        """
        downloads images and stores main data to self.saved_images
        :param images_urls: list of images
        :return:
        """
        print("" + str(len(images_urls)) + " images found")
        for image in images_urls:
            url = url_normalize(image)
            # img is represented by a hash until its classification
            img_hash = self.get_hash()
            try:
                img_path = os.path.join(self.directory, self.directory_data_src, img_hash)
                urllib.request.urlretrieve(url, img_path)

                # check for duplicates
                if not self.is_duplicated(img_path):
                    # img is saved in filesystem represented by hash and data are in self.saved_images
                    self.saved_images.append({'url': url, 'hash': img_hash})
                    print("image retrieved " + url)
                else:
                    os.remove(img_path)

            except:
                self.images_not_reached.append(url)

    def is_duplicated(self, img_path):
        """
        returns true if image is duplicated
        :param img_path: path to image
        :return: boolean
        """
        image_hash = self.hash_image(img_path)
        if image_hash in self.hashed_images:
            return True
        else:
            self.hashed_images.append(image_hash)
            return False

    def classify_by_text(self):
        """
        separates images by criteria "has text in image"
        :return:
        """
        os.mkdir(os.path.join(self.directory, self.directory_text))
        os.mkdir(os.path.join(self.directory, self.directory_no_text))
        print("classifying images by TEXT")
        for image in self.saved_images:
            image_name = os.path.basename(image['url'])
            image_src_path = os.path.join(self.directory, self.directory_data_src, image['hash'])
            directory = self.directory_text if self.has_text(image_src_path) else self.directory_no_text
            self.move_image(image_src_path, directory, image_name)

    def move_image(self, src_path, dest_dir, name):
        """
        moves image from src path to dest directory under specific name
        :param src_path:
        :param dest_dir:
        :param name:
        :return:
        """
        destination_path = os.path.join(self.directory, dest_dir, name)
        shutil.move(src_path, destination_path)

    def classify_by_colors(self):
        """
        moves images to dedicated folders based on the dominant color in it
        :return:
        """
        print("classifying images by COLORS")
        for image in self.saved_images:
            image_src_path = os.path.join(self.directory, self.directory_data_src, image['hash'])
            color_name = self.get_color_name(self.dominant_color(image_src_path))

            color_dir = os.path.join(self.directory, color_name)
            if not os.path.exists(color_dir):
                os.mkdir(color_dir)

            self.move_image(image_src_path, color_name, os.path.basename(image['url']))

    @staticmethod
    def hash_image(image_path):
        """
        returns hash which represents image
        :param image_path:
        :return:
        """
        img = Image.open(image_path).resize((16, 16), Image.LANCZOS).convert(mode="L")
        mean = ImageStat.Stat(img).mean[0]
        return sum((1 if p > mean else 0) << i for i, p in enumerate(img.getdata()))

    @staticmethod
    def get_hash():
        """
        returns random string
        :return: string
        """
        return ''.join(random.choices(string.ascii_letters + string.digits, k=16))

    @staticmethod
    def has_text(filename):
        """
        finds text in image and returns True if there is some
        """
        text = pytesseract.image_to_string(Image.open(filename))
        return text != ''

    @staticmethod
    def dominant_color(filename):
        """
        finds the most used color in the image
        TODO to be improved, because it just find one most used color, but it should focus on histogram
        :param filename:
        :return:
        """
        image = Image.open(filename).convert('RGB')
        pixels = image.getcolors(image.width * image.height)

        sorted_pixels = sorted(pixels, key=lambda t: t[0])
        dominant_color = sorted_pixels[-1][1] if sorted_pixels[-1][1] != (255, 255, 255) else sorted_pixels[-2][1]

        return dominant_color

    @staticmethod
    def get_color_name(rgb_triplet):
        """
        gives a name to selected color. It finds the closest webcolor name
        :param rgb_triplet:
        :return:
        """
        min_colours = {}
        for key, name in webcolors.css21_hex_to_names.items():
            r_c, g_c, b_c = webcolors.hex_to_rgb(key)
            rd = (r_c - rgb_triplet[0]) ** 2
            gd = (g_c - rgb_triplet[1]) ** 2
            bd = (b_c - rgb_triplet[2]) ** 2
            min_colours[(rd + gd + bd)] = name
        return min_colours[min(min_colours.keys())]
