import sys
from src.classifier.classifier import Classifier
from src.scrapper.scrapper import Scraper

if sys.argv.__len__() >= 2:

    if Scraper.is_valid_url(sys.argv[1]) and sys.argv[1][-1] is "/":
        if len(sys.argv) != 3 or sys.argv[2] not in ['text', 'colors']:
            print('unknown operation. Specify "colors" or "text"')
        else:
            scraper = Scraper(sys.argv[1])
            scraper.scrap_page(sys.argv[1])
            classifier = Classifier()
            classifier.get_images(scraper.images_found)

            if sys.argv[2] == 'text':
                classifier.classify_by_text()
            elif sys.argv[2] == 'colors':
                classifier.classify_by_colors()

            print("DONE - check " + classifier.directory)
    else:
        print('unknown operation. Specify "colors" or "text"')
else:
    print("use datasentics.py <url> <operation> - url is absolute web address and operation is 'colors' or 'text'")
