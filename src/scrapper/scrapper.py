import re

import requests
from bs4 import BeautifulSoup
from dnf.pycomp import urlparse


class Scraper:
    main_url = ""
    main_domain = ""
    used_links = []
    links = {}
    images_found = []

    def __init__(self, url):
        self.main_url = url
        self.main_domain = urlparse.urljoin(url, '/')

    def scrap_page(self, url):
        """
        :param url: absolute url
        :return:
        """
        print("scrapping " + url + "...")

        r = requests.get(url, allow_redirects=True, headers={'Accept': 'text/html'})
        self.used_links.append(url)
        self.make_entry(url, r.status_code, None)
        self.get_data(r.content)

    def make_entry(self, link, code, data):
        self.links[link] = {'code': code, 'data': data}

    def is_leaving(self, link):
        """
        checks if the link is going out of the selected domain
        :param link: URL
        :return: bool
        """
        return not re.match(self.main_url + ".*", link)

    def _get_absolute_path(self, link):
        """
        generates a absolute link on this page
        :param link:
        :return:
        """
        if self.is_absolute(link):
            return link
        else:
            if link[0] is "/":
                return self.main_url + link[1:]
            else:
                return self.main_url + link

    def get_data(self, data):
        """
        mines images from html and continues in going deeper in the page
        :param data: html content from the request
        :return:
        """
        soup = BeautifulSoup(data, "lxml")
        links = soup.find_all('a')

        images = soup.find_all('img')
        for image in images:
            image_url = image.get('src', None)
            if self.is_valid_url(image_url):
                self.images_found.append(image_url)

        for tag in links:
            link = tag.get('href', None)
            # check if the URL is valid and we haven't been there yet and go deeper
            if link not in [None, ""]:
                url = self._get_absolute_path(link)
                if self.should_continue(url):
                    self.scrap_page(url)

    @staticmethod
    def is_absolute(url):
        return bool(urlparse.urlparse(url).netloc)

    @staticmethod
    def is_valid_url(link):
        return bool(urlparse.urlparse(link).netloc) \
               and not re.match(".*(mailto|tel|javascript):.*", link)

    def should_continue(self, url):
        """
        checks if there is a sense to
        :param url: string with absolute URL
        :return:
        """
        return self.is_valid_url(url) and url not in self.used_links and not self.is_leaving(url)
